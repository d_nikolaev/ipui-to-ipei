
# IPUI to IPEI converter telegram bot  
Telegram-бот для конвертирования IPUI аппаратов Gigaset в IPEI.   

# Установка и использование (на примере размещения в /usr/local/src под пользователем root)  
## Установка в Linux без использования docker  
### 1. Установить _Python3.6+_  

### 2. Склонировать репозиторий, установить окружение и требуемые библиотеки   
`cd /usr/local/src`  
`git clone https://gitlab.com/d_nikolaev/ipui-to-ipei.git`  
`cd ipui-to-ipei`  
`python3 -m venv venv`  
`source venv/bin/activate`  
`pip install -r requirements.txt`  

### 3. Указать переменную окружения BOT_TOKEN    
_(!) вместо \<TOKEN\> указать корректные данные_  
`export BOT_TOKEN=<TOKEN>`    

### 4. Запустить  
_(!) перед запуском убедиться, что запущено виртуальное окружение (venv)_  
`python bot.py`  

### 5. Создать сервис для работы бота в Linux   
Создать файл сервиса, открыть его на редактирование  
`touch /etc/systemd/system/ipui-bot.service`  
`chmod 664 /etc/systemd/system/ipui-bot.service`  
`vim /etc/systemd/system/ipui-bot.service`  

Вставить в файл следующий текст  
_(!) в поле ExecStart указать другой путь, если программа размещена не в /usr/local/src_  
_(!) вместо \<TOKEN> указать корректные данные_  
`[Unit]`  
`Description=IPUI to IPEI Telegram bot`  
`After=network.target`  
`[Service]`  
`Type=simple`  
`User=root`  
`Environment=BOT_TOKEN=<TOKEN>`  
`ExecStart=/usr/local/src/venv/bin/python /usr/local/src/ipui-to-ipei/bot.py`  
`[Install]`  
`WantedBy=multi-user.target`  

Запустить новый сервис, проверить его статус   
`systemctl start ipui-bot.service`  
`systemctl status ipui-bot.service`  

Добавить его в автозагрузку   
`systemctl enable ipui-bot.service`  

## Установка и использование с помощью docker и docker-compose  

### Docker  
`cd /usr/local/src`  
`git clone https://gitlab.com/d_nikolaev/ipui-to-ipei.git`   
`cd ipui-to-ipei`  
`docker build -t ipui-to-ipei .`   
`docker run -d -e TZ=Europe/Moscow -e BOT_TOKEN=<> ipui-to-ipei`   
_вместо <> указать корректные данные_   
_TZ=Europe/Moscow_ - используется для указания часового пояса GMT+3 Moscow  

### Docker-compose  
`cd /usr/local/src`  
`git clone https://gitlab.com/d_nikolaev/ipui-to-ipei.git`   
`cd ipui-to-ipei`  
`# указать BOT_TOKEN в docker-compose.yml`  
`BOT_TOKEN=<> docker-compose up -d`  
_вместо <> указать корректные данные_   