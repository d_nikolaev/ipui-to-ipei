FROM python:3.6-alpine
WORKDIR /app
RUN apk add --update --no-cache gcc git g++ tzdata
COPY . /app/
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"
RUN pip install --no-cache-dir --upgrade pip && \
pip3 install --no-cache-dir -r /app/requirements.txt && \
chmod +x /app/bot.py
CMD ["python", "/app/bot.py"]