#!./venv/bin/python
# -*- coding: utf-8 -*-
import os
import re  
import telebot  
from ipuitoipei import ipuiToIpei

### (!) предварительно нужно прописать в переменных окружения 
### BOT_TOKEN - токен телеграм-бота (export BOT_TOKEN=<TOKEN>)
bot = telebot.TeleBot(os.environ.get("BOT_TOKEN"))   

@bot.message_handler(commands=['start'])  
def start_message(message):  
    """
    Первый запуск — обработка команды 'start'
    """
    bot.send_message(message.chat.id, 'Hello! I am IPUI to IPEI converter.\nPlease enter IPUI.\n\nex. 019B90BB43')  


@bot.message_handler(commands=['help'])  
def start_message(message):  
    """
    Обработка команды "help"
    (!) В настройках бота у BotFather нужно добавить эту команду (bot - edit bot - edit commands)
    """
    helptext = (f"Every DECT handset has an unique IPUI countber, this is used to easily Register your DECT handsets." + 
            "You can see the IPUI:\n    - On the packaging\n    - In the handset\n\n" + 
            "How to see the IPUI in the handset menu.\n    - Open the handset menu using the right center key.\n    " + 
            "- Open the service menu via * # 06 #\nThe first entry is the IPUI.\n\nExample: 029E74A560"
            )
    bot.send_message(message.chat.id, helptext)  


@bot.message_handler(content_types=['text'])  
def send_text(message):  
    """
    Отправка сообщения с IPUI
    """
    list_ipui = message.text.split()  
    result = ''
    count = 0
    for ipui in list_ipui:
        count += 1
        if len(ipui) == 10:  
            j = 0  
            for character in ipui:
                if not re.match(r'[0-9A-Fa-f]', character):
                    j += 1
            #ipei = 'ERROR: Incorrect IPUI' if j > 0 else ipuiToIpei(ipui)
            ipei = ipuiToIpei(ipui) if j == 0 else 'ERROR: Incorrect IPUI'
        else:  
            ipei = 'ERROR: IPUI - 10 characters'  
        result = f"{result}\n{str(count)}. {ipei}"
    bot.send_message(message.chat.id, result)  
            
            
            
if __name__ == '__main__':  
    bot.infinity_polling()  
