#!./venv/bin/python
# -*- coding: utf-8 -*-
import datetime

def ipuiToIpei(ipui:str = '0370D8FF49'):  
    """
    Функция преобразует IPUI в IPEI   
    """
    try:
        emcHex = ipui[1:5]  
        psnHex = ipui[-5:]  
        emc = str(int(emcHex, 16)).zfill(5)  
        psn = str(int(psnHex, 16)).zfill(7)  
        m = 1 # multiplier  
        chksum = 0  
        emcpsn = emc + psn    
        for c in emcpsn:  
            chksum += int(c)*m  
            m +=1  
        chkdgt = chksum % 11  
        if chkdgt == 10:  
            chkdgt = '*'  
        ipei = str(emc) + str(psn) + str(chkdgt)  
        return ipei  
    except:
        print(f"[{datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S')}] ERROR: Failed to get IPEI")


def main():
    print("Use the function 'ipuiToIpei(ipui)' to convert IPUI to IPEI")

if __name__ == "__main__":
    main()